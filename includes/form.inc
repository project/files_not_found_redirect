<?php

/**
 * @file
 * File for all hooks, belonging to forms.
 */

define("FILES_NOT_FOUND_REDIRECT_CONFIGURATION_ROOT", "configuration");


/**
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form State array.
 * @return mixed
 *   Form with one textfield for the Live-URL
 */

function files_not_found_redirect_configuration_form(array $form, array &$form_state) {

  $form['configuration'] = array(
    '#type' => 'container',
    '#tree' => 'true',
  );

  $form[FILES_NOT_FOUND_REDIRECT_CONFIGURATION_ROOT]['live_url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#default_value' => variable_get('files_not_found_redirect_live_url'),
    '#size' => 128,
    '#maxlength' => 256,
    '#description' => t('The Live-URL of the Website.'),
    '#required' => TRUE,
  );

  $form['actions'] = array(
    'submit' => [
      '#value' => t('Save Configuration'),
      '#type' => 'submit',
      '#validate' => array('files_not_found_redirect_configuration_validation'),
      '#submit' => array('files_not_found_redirect_configuration_submit'),
    ],
  );

  return $form;
}

/**
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form State array.
 */
function files_not_found_redirect_configuration_validation(array $form, array &$form_state) {
  if (!isset($form_state['values']) || empty($form_state['values'])) {
    form_set_error('files_not_found_redirect_configuration_form_error', t('Form does not have values'));
  }

  if (!isset($form_state['values'][FILES_NOT_FOUND_REDIRECT_CONFIGURATION_ROOT]) || empty($form_state['values'][FILES_NOT_FOUND_REDIRECT_CONFIGURATION_ROOT])) {
    form_set_error('files_not_found_redirect_configuration_form_error', t('Form does not have input values'));
  }

  foreach ($form_state['values'][FILES_NOT_FOUND_REDIRECT_CONFIGURATION_ROOT] as $key => $value) {
    switch ($key) {
      case 'live_url':
        if (!valid_url($value, TRUE)) {
          form_set_error('files_not_found_redirect_configuration_form_error', t('Submitted URL is invalid'));
        }
        break;

      default:
        break;
    }
  }
}

/**
 * @param array $form
 *   Form array.
 * @param array $form_state
 *   Form State array.
 */
function files_not_found_redirect_configuration_submit(array $form, array &$form_state) {

  $url = trim($form_state['values'][FILES_NOT_FOUND_REDIRECT_CONFIGURATION_ROOT]['live_url']);

  /*
   * check for trailing slash
   */
  if (substr($url, -1) !== '/') {
    $url .= '/';
  }

  variable_set('files_not_found_redirect_live_url', $url);

}