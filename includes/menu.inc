<?php

/**
 * @file
 * File for all hooks, belonging to menus.
 */

/**
 * Implements hook_menu().
 */
function files_not_found_redirect_menu() {
  $items = array();

  $items['filesnotfoundredirect'] = array(
    'title'            => 'Files not found redirect',
    'position'         => 'right',
    'weight'           => 5,
    'page callback'    => 'files_not_found_redirect_redirect',
    'page arguments'   => [1],
    'access arguments' => ['access content'],
  );

  $items['admin/config/development/files_not_found_redirect'] = array(
    'title' => t('files_not_found_redirect'),
    'description' => t('Main menu item for files_not_found_redirect module'),
    'position' => 'right',
    'weight' => 5,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('files_not_found_redirect_configuration_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}